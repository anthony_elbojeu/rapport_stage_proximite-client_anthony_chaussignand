﻿# Fonction qui ajoute une couleur jaune au message d'erreur "le chemin path n'existe pas".
function Receive-Output 
{
    process { Write-Host $_ -ForegroundColor yellow }
}

# Date et heure qui s'affiche dans les fichiers log.
$Date_log = (Get-Date).ToString('dd-MM-yyyy_HH-mm-ss')


# ---- DEBUT DU SCRIPT ----
# Chemin du dossier qui contiendra les log.
Start-Transcript W:\CLIENTS\log\log-$Date_log.txt

# Chemin du dossier source pour analyser les fichiers RGPD.
$path_files = "W:\CLIENTS\"

# Inclure les formats fichiers dans le traitement des fichiers de donénes. Par défaut : .csv, .xlsx, .xls.
$include_type_files = @("*.csv", "*.xlsx", "*.xls", "*.sql")

# Exlure le dossier "RGPD" qui contient les fichiers de données RGPD traités. Evite d'analyser les fichiers qui sont déjà dans le dossier "RGPD" donc de calculer des fichiers déjà traités.
$exclude_folder = @("z_TRAITEMENTS_RGPD")

# Eviter une erreur si le chemin source "$path_files" n'existe pas ou et faux.
if (-Not (Test-Path $path_files)) {
    Do{
        Write-Output "!!!!!!  Le chemin par défaut $path_files n'existe pas ou n'est pas accessible !!!!!!" | Receive-Output 
        $path_files = Read-Host -Prompt 'Donnez un nouveau chemin au format "mon_chemin" ' 
# Demande à l'utilisateur un chemin tant que le chemin source "$path_files" n'existe pas ou qu'il donne un chemin vide.
    }Until($path_files -and (Test-Path $path_files))
}

# Repertorie l'ensemble des fichiers qui datent de plus de 3 ans et qui ont pour format $include_type_files
$source = Get-ChildItem -Path $path_files  -File  -Recurse -include $include_type_files | Where-Object{$_.FullName -notmatch $exclude_folder} | Where-Object {$_.LastWriteTime -lt ($(Get-Date).AddYears(-3))}

# Chemin destination où sont intégrés les fichiers RGPD traités . Par défaut, dans le dossier "RGPD".
$destination = "W:\CLIENTS\z_TRAITEMENTS_RGPD\"

$counter = 1

foreach($item in $source){
    Write-Verbose "[$counter/$(($source|measure).count)]" -verbose
    Move-Item $item.fullname -Destination $destination -Force -Verbose
    $counter++
}

# Total de fichiers déplacés. On fait -1 car on commence à 1(counter=1) et non à 0.
$counter -= 1  
Write-Output "------ Nombre fichier déplacé est de : $counter ------" 

# ---- FIN DU SCRIPT ----
Stop-Transcript




