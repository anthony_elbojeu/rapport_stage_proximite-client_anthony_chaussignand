import os.path, time
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
import shutil, os


def rgpd_year_deadline(year_default=3):
    return datetime.now() - relativedelta(years=year_default)

def last_modification_ostime(path_file) -> float:
     while True:
        try:
            os_time_file = os.path.getmtime(path_file)
            return os_time_file
        except OSError:
            print("Le chemin '%s' n'existe pas ou n'est pas accessible" %path_file)
            path_file = str(input("Donnez un nouveau chemin au format: monchemin . Par défaut le chemin est /home/anthony/Documents/STAGE ANTHONY/Developpement_code/exemple_dossier_client/."))

def ostime_to_strftime(ostime_file) -> int:
    strftime_file = datetime.fromtimestamp(ostime_file).strftime('%Y-%m-%d %H:%M:%S')
    return strftime_file

def strftime_to_datetime(strftime_file) -> int:
    datetime_file = datetime.strptime(strftime_file, '%Y-%m-%d %H:%M:%S')
    return datetime_file

def moove_file(source, destination, filename):
    # Permet de joindre url dossier + nom_fichier
    # Evite les erreurs : le fichier dans la destination existe déjà
    dest = os.path.join(destination, filename)
    return shutil.move(source, dest)
 
def rgpd_file_deadline_exceeded(path="/home/anthony/Documents/STAGE ANTHONY/Developpement_code/exemple_dossier_client/.", path_destination= "dossier_rgpd", years=3):
    start_time = time.time()
    nombre_fichier_deplacer = 0
    liste_extensions_verif = [".xlsx", ".xls", ".csv"]
    for dirname, dirnames, filenames in os.walk(path):
        # Fonction qui calcule la date excate il y a 3 ans par défaut.
        deadline= rgpd_year_deadline(years)
        for filename in filenames:
            path_file = os.path.join(dirname, filename)
            # Vérifier si l'extansion est xlsx.
            if os.path.splitext(filename)[1] in liste_extensions_verif:
                # dirname permet de voir le chemin du fichier
                last_modification_file_os = last_modification_ostime(path_file)
                # Je transforme le résultat ostime en strtime
                last_modification_file_str = ostime_to_strftime(last_modification_file_os)
                # Je transforme le résultat timestamp en datetime
                last_modification_file_datetime = strftime_to_datetime(last_modification_file_str)
                # Je calcule la date de création du document rapport à la date de la deadline RGPD
                if (last_modification_file_datetime < deadline):
                    moove_file(path_file, path_destination, filename)
                    nombre_fichier_deplacer +=1
    print("---Le temps d'exécution est  %s seconds ---" % (time.time() - start_time))
    print(nombre_fichier_deplacer, "fichiers déplacés")


if __name__ == "__main__":
    rgpd_file_deadline_exceeded()